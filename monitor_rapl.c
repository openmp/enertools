
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <likwid.h>
#include <stdint.h>
#include <inttypes.h>

#include <time.h>
#include <sys/time.h>

#if 1
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_seconde(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_seconde_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_seconde(t) (1e-6*(double)t.tv_usec)
#  define get_sub_seconde_ns(t) (1000*(uint64_t)t.tv_usec)
#endif

//static int sleeptime = 500*1e3;

// configure sleep time (100ms)
struct timespec sleeptime = {0, 100000000UL };
struct timespec rem = {0,0};

static int run = 1;

void  INThandler(int sig)
{
    signal(sig, SIG_IGN);
    run = 0;
}

uint64_t kaapi_get_elapsedms(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000ULL;
  retval += (uint64_t)(get_sub_seconde_ns(st)* 1000ULL);
  return retval;
}

uint64_t kaapi_get_elapsedns(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000000ULL;
  retval += get_sub_seconde_ns(st);
  return retval;
}

int main (int argc, char* argv[])
{
    int i, c, err = 0;
    int s= 0;
    double timer = 0.0;
    topology_init();
    numa_init();
    affinity_init();
    timer_init();
    int total_cpu = sysconf( _SC_NPROCESSORS_ONLN );
    CpuInfo_t cpuinfo = get_cpuInfo();
    CpuTopology_t cputopo = get_cpuTopology();
    int numCPUs = cputopo->activeHWThreads;
    // keeps all cpu ids
    int* cpus = malloc(numCPUs * sizeof(int));
    // keeps the first core on each socket
    int* sockets = malloc(cputopo->numSockets* sizeof(int));

    char* pathname = "energy_rapl.csv"; // = argv[1];
    int opt;
    int ms;
    while ((opt = getopt(argc, argv, "hf:d:")) != -1) {
      switch (opt) {
        case 'h':
          printf("Usage: %s [-h]|[-f filename]|[-d period]\n", argv[0]);
          printf("  * -h: this help\n");
          printf("  * -f filename: output values into file 'filename'. Data is appended at end of existing file.\n");
          printf("  * -d period: the period in ms between two successive measures\n");
          exit(0);
          break;
        case 'd':
          ms = atoi(optarg);
          sleeptime.tv_nsec = ms*1000000L;
          break;
        case 'f':
          pathname = strdup(optarg);
          break;
      }
    }
    FILE* foutput = fopen(pathname, "a");
    if (foutput ==0)
    {
       fprintf(stderr,"*** Error, cannot open file '%s'\n", pathname);
       exit(1);
    }
    
    printf("delay set to %i(ms)\n", sleeptime.tv_nsec/1000000L);
    printf("writing to %s ...\n", pathname);

    if (!cpus || !sockets)
    {
        affinity_finalize();
        numa_finalize();
        topology_finalize();
        return 1;
    }
    c = 0;
    // initialize sockets
    for (i= 0; i < cputopo->numSockets; i++)
      sockets[i] = -1;
    for (i=0;i<cputopo->numHWThreads;i++)
    {
        if (cputopo->threadPool[i].inCpuSet)
        {
            cpus[c] = cputopo->threadPool[i].apicId;
            // it seems that we are able to read RAPL only from the 1st core
            if( sockets[cputopo->threadPool[i].packageId] < 0)
                sockets[cputopo->threadPool[i].packageId] = cpus[c];
            c++;
        }
    }
    NumaTopology_t numa = get_numaTopology();
    AffinityDomains_t affi = get_affinityDomains();
    timer = timer_getCpuClock();

    perfmon_init(cputopo->numSockets, sockets);

    char counters[] = "PWR_PKG_ENERGY:PWR0,PWR_PP0_ENERGY:PWR1,PWR_DRAM_ENERGY:PWR3,TEMP_CORE:TMP0";
    int gid3 = perfmon_addEventSet(counters);
    if (gid3 < 0)
    {
        printf("Failed to add performance group ENERGY\n");
        err = 1;
        goto monitor_exit;
    }
    signal(SIGINT, INThandler);

  fprintf(foutput, "Start,Stop,Duration,Socket,CPU,");
    char* ptr = strtok(counters, ",");
  while(ptr != NULL){
    fprintf(foutput, "%s,", ptr);
              ptr = strtok(NULL, ",");
  }
  fprintf(foutput, "\n");
  
    while (run)
    {
        perfmon_setupCounters(gid3);
        uint64_t t0 = kaapi_get_elapsedns();
        perfmon_startCounters();
//        usleep(sleeptime);
//        nanosleep(&sleeptime, NULL);
        clock_nanosleep(CLOCK_REALTIME, 0, &sleeptime, &rem);
        perfmon_stopCounters();
        uint64_t t1 = kaapi_get_elapsedns();
        uint64_t tdelta = (uint64_t)(t1-t0);
        for (s = 0; s < cputopo->numSockets; s++)
        {
          // get the 1st core from this socket
          c = sockets[s];
          fprintf(foutput, "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%d,%d,", t0, t1, tdelta, s, sockets[s]);
          for (i = 0; i< 4; i++)
          {
                // this one gets the result accumulated
                //fprintf(foutput, "%f,", perfmon_getResult(gid3, i, sockets[s]));
                // this one gets the last result (not metric)
                fprintf(foutput, "%f,", perfmon_getLastResult(gid3, i, s));
          }
          fprintf(foutput, "\n");
        }
    }
monitor_exit:
    fclose(foutput);
    printf("end %s ...\n", pathname);
    free(cpus);
    free(sockets);
    perfmon_finalize();
    affinity_finalize();
    numa_finalize();
    topology_finalize();
    return 0;
}
