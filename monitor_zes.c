#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdint.h>

#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#include <time.h>
#include <sys/time.h>

#if 1
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_seconde(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_seconde_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_seconde(t) (1e-6*(double)t.tv_usec)
#  define get_sub_seconde_ns(t) (1000*(uint64_t)t.tv_usec)
#endif

/* connection port for Zimmer wattmeter */
#define ZES_TTYUSB "/dev/ttyUSB1"

static int volatile run = 1;
int   fd_serial   =0;
FILE* fout_energy =0;
int verbose = 0;


/* List of commands to initialize LMG450 */
const char* list_cmd[] = {
  "*CLS",
  "*RST",
  ":SYST:LANG SHORT",
  "FRZ",
  "CYCL",
  "TIME",
  "RESET",
  "START",
  "ACTN;*TST? 20; *TST? 21; P?; EP?",
  "FRMT PACKED",
  "CONT ON",
  0
};


/* List of commands to finalize LMG450 */
const char* list_cmdend[] = {
  "CONT OFF",
  "STOP",
  "FRMT ASCII",
  "GTL",
  0
};


/* Initialization of serial port */
int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}


uint64_t kaapi_get_elapsedns(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000000ULL;
  retval += get_sub_seconde_ns(st);
  return retval;
}


typedef struct flowbuff {
  int fd;
  int rdlen;
  unsigned char buf[256];
  unsigned char* pos;
} flowbuff;

void init_flowbuff( flowbuff* fb, int fd )
{
  fb->fd = fd;
  fb->rdlen = 0;
  fb->pos = 0;
}

/* returns byte or block until next char 
   - in case of error -1 is returned and errno is set
*/
int getchar_fromflow( flowbuff* fb )
{
restart:
  if (fb->rdlen >0)
  {
    unsigned char retval = *fb->pos;
    ++fb->pos; 
    --fb->rdlen;
//printf("char:%i %c\n", (int)retval, retval);
    return retval;
  }

  int rdlen;
  do {
    rdlen = read(fb->fd, fb->buf, sizeof(fb->buf) - 1);
    if (rdlen == -1) 
    {
      printf("*** error: %i, msg='%s'\n", errno, strerror(errno));
      return -1;
    }
//    printf(" rdlen=%i\n", rdlen);
  } while (rdlen ==0);
  fb->rdlen = rdlen;
  fb->pos = &fb->buf[0];
  goto restart;
}


/* decode Zimmer binary: '#' has been read 
   return the number of 4Bytes values (int32/uint32 or float) 
*/
int decodebinary( flowbuff* fb, void* array, int n )
{
  int i;
  typedef union {
    int32_t i;
    uint32_t u;
    float f;
    unsigned char p[4];
  } anonymous_bytesvalue;
  int l = getchar_fromflow(fb);
  if (l == -1) return -1;
  l = l - '0';
  int digits = 0;
#if LOG
  printf("#len=%i\n", l);
#endif
  for (i=0; i<l; ++i)
  {
     int d = getchar_fromflow(fb);
     if (d == -1) return -1;
     if (d == 10) return -1;
#if LOG
     if (!isdigit(d))
        printf("Error: digit: %c %i\n", (char)d, (int)d );
     else 
#endif
     {
#if LOG
       printf("digit: %c %i\n", (char)d, (int)d );
#endif
       digits = 10*digits + (d-'0');
     }
  }
#if LOG
  printf("Read #digits=%i\n", (int)digits);
#endif 
  unsigned char* curr = (unsigned char*)array; 
  for (i=0; i<digits; ++i)
  {
     int c = getchar_fromflow(fb);
     if (c == -1) return -1; 
     *curr++ = c;
  }
  return digits/4;
}




/*
*/
int send_cmd( int fd, const char** list_cmd, float period)
{
  int i,wlen,len;
  ssize_t sz;
  char buffer[128];

  /* output command to the zimmer */
  for (i=0; list_cmd[i] !=0; ++i)
  {
     
     if (strncmp(list_cmd[i], "CYCL", 4) ==0)
     {
       if (period <= 0.05)
         continue;
       sz = sprintf(buffer, "%s %f\r", list_cmd[i], period);
     }
     else if (strncmp(list_cmd[i], "TIME", 4) ==0)
     {
       time_t timep = time(0);
       struct tm *tmp = gmtime(&timep);
       if (tmp ==0)
         continue;
       else
       {
         sz = sprintf(buffer, "%s %u,%u,%u\r", list_cmd[i], tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
       }
     }
     else
     {
       sz = sprintf(buffer, "%s\r", list_cmd[i]);
     }

     len = strlen(buffer);
     wlen = write(fd, buffer, len);
     if (wlen != len) {
       printf("*** error from write: %d, %d\n", wlen, errno);
       return -1;
     }
     tcdrain(fd);    /* delay for output */
  }
}


/*
*/
void INThandler(int sig)
{
  fclose(fout_energy);
  if (verbose) printf("Interrup");
  send_cmd( fd_serial, list_cmdend, 0);
  printf("end capture\n");
  close(fd_serial);
  fclose(fout_energy);
  run = 0;
  exit(1);
}


/*
*/
int main(int argc, char** argv)
{
  char *portname = ZES_TTYUSB;
  char buffer[256];

  int alreadyexist; 
  char* pathname = 0;
  float period = 0.05;
  int opt;
  int dump = 0;

  while ((opt = getopt(argc, argv, "hf:d:gv")) != -1) {
    switch (opt) {
       case 'h':
         printf("Usage: %s [-h]|[-f filename]|[-d period]\n", argv[0]);
         printf("Configure and capture power and energy values from Zimmer wattmeter connected on '" ZES_TTYUSB "'\n");
         printf("  * -h: this help\n");
         printf("  * -f filename: output values into file 'filename'. Data is appended to a possible existing file.\n");
         printf("  * -d period: the period in ms between two successive measures\n");
         printf("  * -v : activate verbose mode to dump on stdout each new data\n");
         exit(0);
         break;
      case 'v':
        dump = 1;
        break;
      case 'd':
        period = (float)atoi(optarg) /1000.0;
        if (period < 0.05) period = 0.05;
        break;
      case 'f':
        pathname = strdup(optarg);
        break;
      case 'g':
        verbose=1;
        printf("Verbose on\n");
        break;
    }
  }

  /* Open */
  if (verbose) printf("Open %s serial port...", portname );
  fd_serial = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd_serial < 0) {
     if (verbose) printf("... error\n");
     printf("Error opening %s: %s\n", portname, strerror(errno));
     return -1;
  }

  /*baudrate 230400, 8 bits, no parity, 1 stop bit */
  set_interface_attribs(fd_serial, B230400);
  //set_mincount(fd_serial, 1);                /* set to pure timed read */

  flowbuff fb;
  init_flowbuff( &fb, fd_serial );
  if (verbose) printf("... fd:%i  ok\n", fd_serial);

  if (pathname ==0) pathname = "energy.csv";
  if (access(pathname, W_OK) ==0)
    alreadyexist=1;
  else
    alreadyexist=0;
  
  fout_energy = fopen(pathname, "a");
  //void* fout_buffer = malloc( 4*getpagesize() );
  //setvbuf( fout_energy, fout_buffer, _IOFBF, 4*getpagesize() );

  printf("[monitor_zes] delay set to %i(ms)\n", (int)(period*1000)); 

  signal(SIGINT, INThandler);
  signal(SIGTERM, INThandler);

  /* send init cmd for the zimmer */
  if (verbose) printf("Send cmd to Zimmer");
  send_cmd(fd_serial, list_cmd, period);
  if (verbose) printf("... ok\n");

  /* simple noncanonical input */
  if (!alreadyexist)
    fprintf(fout_energy,"Stamp, StampZES, P, dE, E\n");

  printf("[monitor_zes] writing to %s ...\n", pathname);

  int cnt = 0;
  uint64_t timestamp;
  uint64_t lasttimestamp = kaapi_get_elapsedns();
  int first_out = 0;
  float last_energy = 0;
  while (run) 
  {
    /* skip every thing until '#' */
    int p;
    for (p = getchar_fromflow(&fb); (p != '#') && (p != -1); p = getchar_fromflow(&fb) )
      usleep(1000);

    /* decode */
    struct {
      uint32_t H;
      uint32_t L;
      float    P;
      float    E;
    } point;

    int n = decodebinary(&fb, (void*)&point, 4);
    if (++cnt < 20) continue;
    if ((n >0) & (point.E >=0) & (point.P >=0))
    {
      uint64_t timestampZimmer = 1000ULL*(point.H * (((uint64_t)1) << 32ULL) + point.L);
      timestamp = kaapi_get_elapsedns();
      if (last_energy >0)
      {
        snprintf(buffer, 256, "%" PRIu64 ", %" PRIu64 ", %f, %f, %f\n", timestamp, timestampZimmer, 
           point.P, 
           (point.E-last_energy)*3600,
           point.E*3600
        );
        fprintf(fout_energy,buffer);
        if (dump||verbose)
          fprintf(stdout,buffer);
      }
      last_energy = point.E;
      if (first_out ==0)
      {
        first_out = 1;
        printf("starting capture... delay(s): %.2f\n", (timestamp-lasttimestamp)/(double)1000000000UL);
      }
#if 0
      if (timestamp-lasttimestamp > 1UL*60UL*1000000000UL)
      {
        fflush(fout_energy);
        lasttimestamp = timestamp;
        if (verbose)
          fprintf(stdout,"flush buffer\n");
      }
#endif
    }
    else 
      // means that forget to decode binary
      if (verbose) printf(".");

    /* repeat read to get full message */
  }
  printf("end capture\n");

  if (verbose) printf("Send cmd to finalize Zimmer");
  /* output command to finalize the zimmer */
  send_cmd(fd_serial, list_cmdend, 0 ); 
  if (verbose) printf("... ok\n");

  fclose(fout_energy);
}
