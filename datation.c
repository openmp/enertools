#include <stdio.h>
#include <inttypes.h>
#include <stdint.h>

#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#include <time.h>
#include <sys/time.h>

#if 1
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_seconde(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_seconde_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_seconde(t) (1e-6*(double)t.tv_usec)
#  define get_sub_seconde_ns(t) (1000*(uint64_t)t.tv_usec)
#endif


/*
*/
uint64_t kaapi_get_elapsedns(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000000ULL;
  retval += get_sub_seconde_ns(st);
  return retval;
}


/*
*/
int main(int argc, char** argv)
{
  printf("%" PRIu64, kaapi_get_elapsedns());
  return 0;
}
